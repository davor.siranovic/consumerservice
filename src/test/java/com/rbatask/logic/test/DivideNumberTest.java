package com.rbatask.logic.test;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.rbatask.logic.DivideNumberService;

@SpringBootTest
class DivideNumberTest {

	@Test
	void testRadnomData() {
		DivideNumberService dns = new DivideNumberService();
		assertEquals("RBA", dns.divideNumber(15));
		assertEquals("R3", dns.divideNumber(9));
		assertEquals("B", dns.divideNumber(25));
		assertEquals("", dns.divideNumber(0));
		assertEquals("-6", dns.divideNumber(0));
	}

}
