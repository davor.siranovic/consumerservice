package com.rbatask.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class InMemoryDatabase {
	
	private static final Logger logger = LogManager.getLogger(InMemoryDatabase.class);
	
	private static final String DB_DRIVER = "org.h2.Driver";
	private static final String DB_CONNECTION = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";
	
	public static Connection con = null;
	public static Statement stmt = null;
	
	public static void init() {
		try {
			Class.forName("org.h2.Driver");
			con = DriverManager.getConnection("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "sa", "");
			stmt = con.createStatement();

			// Create data table
			stmt.executeUpdate("CREATE TABLE WORDS (word VARCHAR(255), timestamp TIMESTAMP)");
			
			// Create config table 
			stmt.executeUpdate("CREATE TABLE CONFIG (CONFIGURABLE VARCHAR(255), VALUE INT ,timestamp TIMESTAMP)");
			
			//INSERT CONFIGURABLES
			stmt.executeUpdate("INSERT INTO CONFIG (configurable, value, timestamp) VALUES ('RANDOM_NUMBERS', 200 ,NOW())");
			stmt.executeUpdate("INSERT INTO CONFIG (configurable, value, timestamp) VALUES ('WAIT', 10 ,NOW())");


		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
	
	public static JSONArray select() {
		JSONArray result = new JSONArray();
		try {
			ResultSet rs =  stmt.executeQuery("SELECT word, count(*) as counter FROM WORDS GROUP BY WORD");
			
		       while(rs.next()){
		            //Retrieve by column name
		            String word  = rs.getString("word");
		            String counter = rs.getString("counter");
		            
					JSONObject row = new JSONObject();
					row.put("word", rs.getString("word"));
					row.put("counter", rs.getString("counter"));

					result.put(row);
		            
					logger.info("DB row word "+ word + " counter " + counter);
		        }
		        rs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return result;
	}
	
	public static JSONArray selectR() {
		JSONArray result = new JSONArray();
		try {
			ResultSet rs =  stmt.executeQuery("SELECT word, count(*) as counter FROM WORDS where word = 'R' GROUP BY WORD");
			
		       while(rs.next()){
		            //Retrieve by column name
		            String word  = rs.getString("word");
		            String counter = rs.getString("counter");
		            
					JSONObject row = new JSONObject();
					row.put("word", rs.getString("word"));
					row.put("counter", rs.getString("counter"));
					
					result.put(row);
		            
					logger.info("DB row word "+ word + " counter " + counter);
		        }
		        rs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}	
		return result;
	}
	
	public static JSONArray selectB() {
		JSONArray result = new JSONArray();
		try {
			ResultSet rs =  stmt.executeQuery("SELECT word, count(*) as counter FROM WORDS where word = 'B' GROUP BY WORD");
			
		       while(rs.next()){
		            //Retrieve by column name
		            String word  = rs.getString("word");
		            String counter = rs.getString("counter");
		            
					JSONObject row = new JSONObject();
					row.put("word", rs.getString("word"));
					row.put("counter", rs.getString("counter"));
					
					result.put(row);
		            
					logger.info("DB row word "+ word + " counter " + counter);
		        }
		        rs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return result;
	}
	
	public static JSONArray selectRBA() {
		JSONArray result = new JSONArray();
		try {
			ResultSet rs = stmt
					.executeQuery("SELECT word, count(*) as counter FROM WORDS where word = 'RBA' GROUP BY WORD");

			while (rs.next()) {
				// Retrieve by column name
				String word = rs.getString("word");
				String counter = rs.getString("counter");

				JSONObject row = new JSONObject();
				row.put("word", rs.getString("word"));
				row.put("counter", rs.getString("counter"));

				result.put(row);

				logger.info("DB row word " + word + " counter " + counter);
			}
			rs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return result;
	}
	
	public static int selectNumber() {
		int result = 0;
		try {
			ResultSet rs = stmt
					.executeQuery("SELECT value FROM CONFIG where configurable = 'RANDOM_NUMBERS'");

			while (rs.next()) {
				// Retrieve by column name
				result = rs.getInt("value");
			}
			rs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return result;
	}
	
	public static int selectWait() {
		int result = 0;
		try {
			ResultSet rs = stmt
					.executeQuery("SELECT value FROM CONFIG where configurable = 'WAIT'");

			while (rs.next()) {
				// Retrieve by column name
				result = rs.getInt("value");
			}
			rs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return result;
	}
	
	public static void insert(String word) {
		// Insert non empty word data
		if (null != word && !word.equals("") && !word.equals("")) {
			try {
				stmt.executeUpdate("INSERT INTO WORDS (word, timestamp) VALUES ('" + word + "', NOW())");
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	public static void closeConnection(String word) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public static void main(String[] args) {
		Connection con = null;
		Statement stmt = null;

		try {
			Class.forName("org.h2.Driver");
			con = DriverManager.getConnection("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "sa", "");
			stmt = con.createStatement();

			// Create table
			stmt.executeUpdate("CREATE TABLE WORDS (word VARCHAR(255), timestamp TIMESTAMP)");

			// Insert data
			stmt.executeUpdate("INSERT INTO WORDS (word, timestamp) VALUES ('hello', NOW())");
			stmt.executeUpdate("INSERT INTO WORDS (word, timestamp) VALUES ('world', NOW())");
			System.out.println("all good");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}
}
