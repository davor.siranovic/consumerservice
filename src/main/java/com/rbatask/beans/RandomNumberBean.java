package com.rbatask.beans;

public class RandomNumberBean {
	private int r1;
	
	public RandomNumberBean() {
		super();
	}

	public RandomNumberBean(int r1) {
		super();
		this.r1 = r1;
	}

	public int getR1() {
		return r1;
	}

	public void setR1(int r1) {
		this.r1 = r1;
	}
}
