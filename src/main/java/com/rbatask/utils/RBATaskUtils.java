package com.rbatask.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class RBATaskUtils {
	
	
	public static String JSONArrayToString(JSONArray returnArray) {
		String result = "";
		
	    // Creating example List and adding the data to it
	    List<JSONObject> exampleList = new ArrayList();
	    for (int i = 0; i < returnArray.length(); i++) {
	        exampleList.add(returnArray.getJSONObject(i));
	    }
	    
	    // Creating String array as our
	    // final required output
	    int size = exampleList.size();
	    String[] stringArray
	        = exampleList.toArray(new String[size]);

	    // Printing the contents of String array
	    System.out.print("Output String array will be : ");
	    for (String s : stringArray) {
	    	result += s + " ";
	    }
	    
		return result;
	}





}
