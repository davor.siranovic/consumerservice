package com.rbatask.random;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.rbatask.logic.GetRandomData;


@SpringBootApplication
public class RandomNumberConsumer {
	private static final Logger logger = LogManager.getLogger(RandomNumberConsumer.class);
	public static void main(String[] args) {
		SpringApplication.run(RandomNumberConsumer.class, args);
		try {
			GetRandomData grd = new GetRandomData();
			grd.getRandomData();
		} catch (Exception e) {
			logger.error("The random number cannot be consumed");
		}
	}

}
