package com.rbatask.random;

import java.util.Random;

import org.json.JSONArray;
import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rbatask.beans.RandomNumberBean;
import com.rbatask.storage.InMemoryDatabase;
import com.rbatask.utils.RBATaskUtils;

@RestController
public class RandomNumberConsumerResoult {
	@GetMapping("/randomNumberConsumerResoult")
	public String getDemoBeanOut() {
		return InMemoryDatabase.select().toString();
	}
	
	@GetMapping("/RBA")
	public String getRBAOut() {
		return InMemoryDatabase.selectRBA().toString();
	}
	
	@GetMapping("/R")
	public String getROut() {
		return InMemoryDatabase.selectR().toString();
	}
	
	@GetMapping("/B")
	public String getBOut() {
		return InMemoryDatabase.selectB().toString();
	}
}
