package com.rbatask.logic;

public class DivideNumberService {
    public String divideNumber(int randomNumber) {
        if (randomNumber % 3 == 0 && randomNumber % 5 == 0) {
            return "RBA";
        }
        if (randomNumber % 3 == 0) {
            return "R"; 
        }
        if (randomNumber % 5 == 0) {
        	return "B"; 
        }
        return "";
    }
}
