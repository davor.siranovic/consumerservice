package com.rbatask.logic;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rbatask.beans.RandomNumberBean;
import com.rbatask.random.RandomNumberConsumer;
import com.rbatask.storage.InMemoryDatabase;

public class GetRandomData {
	
	private static final Logger logger = LogManager.getLogger(GetRandomData.class);
	
	private InMemoryDatabase imd = new InMemoryDatabase();
	
	public InMemoryDatabase getImd() {
		return imd;
	}

	public void setImd(InMemoryDatabase imd) {
		this.imd = imd;
	}

	public void getRandomData() {
		initDataStorage();
		int numberOfRandoms = InMemoryDatabase.selectNumber();
		
		String url = "http://localhost:8081/randomNumber";
		RestTemplate rt = new RestTemplate();
		
		for (int i = 0; i < numberOfRandoms; i++) {
			delay();
			String numberString = rt.getForObject(url, String.class);

			// create ObjectMapper instance
			ObjectMapper objectMapper = new ObjectMapper();

			try {
				// read JSON file and convert to a customer object
				RandomNumberBean rnb = objectMapper.readValue(numberString, RandomNumberBean.class);
				storeTheNumber(rnb.getR1());
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			logger.info("Random number " + numberString);
		}
	}
	
	public void delay() {
		int wait = InMemoryDatabase.selectWait();
		try {
		    Thread.sleep(wait);
		} catch (InterruptedException ie) {
		    Thread.currentThread().interrupt();
		}		
	}
	
	public void storeTheNumber(int randomNumber) {
    	DivideNumberService dns = new DivideNumberService();
    	String stringToStore = dns.divideNumber(randomNumber);
    	imd.insert(stringToStore);
	}
	
	public void initDataStorage() {
		imd.init();
	}
}

