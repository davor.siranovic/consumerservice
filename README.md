# consumer service

This is a second service from RBA task that consumes random numbers and deals 
with them as specified:

Drugi servis:

1.       Zove prvi servis obrađuje brojeve ih na način da ako je broj iz poruke djeljiv s 3, sprema string "R" u storage, a ako je broj is poruke djeljiv s 5, sprema "B" u storage, a ako je djeljiv i s 3 i s 5 servis, onda  string „RBA“ sprema u storage. Slova se trebaju spremiti sa timestamp-om.

2.       Također izlaže preko RESTa endpoint koji na upit (koji može sadržavati string „R“, „B“, odnosno „RBA“), vraća count odgovarajuceg stringa iz storage. Npr. Ako upit sadrži „R“, onda servis treba vratiti koliko ima slova „R“ u storage-u.

To analyze and run the consumer service in can be cloned from this address:

https://gitlab.com/davor.siranovic/consumerservice

To start the service it is needed RandomNumberConsumer to run as Java app. 
The service is consuming random number from the random_producer servis, so 
it is needed this service to be ran previous.

This service runs on localhost:8082

It's outputs are:

http://localhost:8082/randomNumberConsumerResoult
http://localhost:8082/RBA
http://localhost:8082/R
http://localhost:8082/B

Main logic is in the GetRandomData

It uses in memory database InMemoryDatabase - for running no additional settings are needed 
except that defined in the pom.xml file 

Configuration (number of random numbers to be processed and delay time in milliseconds) 
is done in the database CONFIG table:

			INSERT INTO CONFIG (configurable, value, timestamp) VALUES ('RANDOM_NUMBERS', 200 ,NOW())"
			INSERT INTO CONFIG (configurable, value, timestamp) VALUES ('WAIT', 10 ,NOW())"



